const fs = require("fs")

let severityValues = ["info", "minor", "major", "critical", "blocker"]

if (fs.existsSync("./demo.quality.js"))  {
  let hotSpots = [
      {
          description: "Too long string",
          fingerprint: "001",
          location: {
              path: "./demo.js",
              lines: {
                  begin: 3
              }
          },
          severity: severityValues[1]
      },
      {
          description: "Usage of console.log is not recommended",
          fingerprint: "002",
          location: {
            path: "./demo.js",
            lines: {
                  begin: 6
              }
          },
          severity:severityValues[0]
      }
      ,
      {
          description: "Usage of console.log is not recommended",
          fingerprint: "003",
          location: {
            path: "./demo.js",
            lines: {
                  begin: 8
              }
          },
          severity:severityValues[0]
      }
  ]

  fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(hotSpots, null, 2))
}


